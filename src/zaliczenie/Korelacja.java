/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zaliczenie;


/**
 *
 * @author Komputer
 */
public class Korelacja {
    
    private int x[];
    private int y[];
 
public Korelacja(int rozm)
{
    this.x = new int[rozm];
    this.y = new int[rozm];


}    
    

    
private double Correlation(int[] xs, int[] ys) {
    //TODO: check here that arrays are not null, of the same length etc

    double sx = 0.0;
    double sy = 0.0;
    double sxx = 0.0;
    double syy = 0.0;
    double sxy = 0.0;

    int n = xs.length;

    for(int i = 0; i < n; ++i) {
      double x = xs[i];
      double y = ys[i];

      sx += x;
      sy += y;
      sxx += x * x;
      syy += y * y;
      sxy += x * y;
    }

    // covariation
    double cov = sxy / n - sx * sy / n / n;
    // standard error of x
    double sigmax = Math.sqrt(sxx / n -  sx * sx / n / n);
    // standard error of y
    double sigmay = Math.sqrt(syy / n -  sy * sy / n / n);

    // correlation is just a normalized covariation
    return cov / sigmax / sigmay;
  }

public double policz(Panstwa panstwo[])
{
    double kor = 0;
    int rozm = panstwo.length;
    
    for( int i=0 ; i<rozm ; i++)
    {
        x[i] = panstwo[i].dlZycia;
        y[i] = panstwo[i].SrZar;
    }
    return Correlation(this.x,this.y);
}



}
