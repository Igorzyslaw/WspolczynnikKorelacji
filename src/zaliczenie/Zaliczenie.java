/*
 * Program sprawdzający zależność między dlugością życia a jego poziomem
 */
package zaliczenie;

import java.io.IOException;


/**
 *
 * @author Igor Stawiński 87737
 */
public class Zaliczenie {

    
    public static void main(String[] args) throws IOException {
            plik plik = new plik();
            ///////////////////////////
            ///////////////////////////
            
            System.out.println("######################################################################");
            System.out.println("#########Program sprawdzajacy czy zachodzi korelacja miedzy ##########");
            System.out.println("#dwoma zestawami danych - SREDNIA DL ZYCIA i DOCHOD NA MIESZKANCA#####");
            System.out.println("######################################################################");
            System.out.println();
            System.out.println();
            
            plik.wczytajSciezke(1);
            plik.wczytajSciezke(2);
            System.out.println();
            System.out.println("# Wczytalem obydwa pliki.");
            ///////////////////////////
            ///////////////////////////  
            int ileWpisow1[] = plik.policzWiersze(plik.plik1);
            int ileWpisow2[] = plik.policzWiersze(plik.plik2);
            System.out.println("# Pierwszy plik zawiera: "+ileWpisow1[0] + " poprawnych oraz: "+ileWpisow1[1]+" niepoprawnych wpisów.");
            System.out.println("# Drugi plik zawiera: "+ileWpisow2[0] + " poprawnych oraz: "+ileWpisow2[1]+" niepoprawnych wpisów.");
            System.out.println();
            ///////////////////////////
            ///////////////////////////  
            System.out.println("# Przepisuje wpisy do tablic...");
            tablice tablica1 = new tablice(ileWpisow1[0],plik.plik1);
            tablice tablica2 = new tablice(ileWpisow2[0],plik.plik2);
            System.out.println();
            ///////////////////////////
            ///////////////////////////             
            System.out.println("# Dane zostały umieszczone w tablicach.");
            System.out.println("# Rozpoczynam sprawdzanie czy wszystkie Państwa mają komplet danych...");
            ///////////////////////////
            /////////////////////////// 
            int ilePar = tablica1.sprIleOK(tablica1, tablica2);
            System.out.println("# " + ilePar + " państw posiada dane w obydwu plikach. Scalam zebrane informacje.");
            ///////////////////////////
            /////////////////////////// 
            Panstwa panstwo[] = new Panstwa[ilePar];
            tabIni(panstwo);// inicjalizacja tablicy
            panstwo = scal(tablica1,tablica2,panstwo); // scalenie danych oraz przypisanie ich do tablicy panstwo
            //////////////////////////////////////////////
            /////////////////Korelacja////////////////////
            //////////////////////////////////////////////
            Korelacja korelacja = new Korelacja(ilePar);
            System.out.println();
            System.out.println("# Obliczam wspolczynnik korelacji dla wczytanych wartosci...");
            System.out.println();
            System.out.println("#####################################################");
            System.out.format("#  Wspolczynnik korelacji:           %.3f          #",korelacja.policz(panstwo));
            System.out.println();
            System.out.println("#####################################################");
            
            

            //wyswietlDane(panstwo);
            
            
      }
    
    
    ////////////////////////////////////////////////////
    ///////////////////////Metody///////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    
    static void wyswietlDane(Panstwa[] panstwo)
    {
        for(int p=0;p<panstwo.length;p++){ // inicjalizacja oraz scalenie danych
                System.out.println((p+1) + ". " + panstwo[p].Nazwa + " " + panstwo[p].dlZycia  + " " + panstwo[p].SrZar );
            }
    }
    
    static void tabIni(Panstwa[] panstwo)
    {
        for(int p=0;p<panstwo.length;p++){ // inicjalizacja 
                panstwo[p] = new Panstwa(); }
    }
    
    static Panstwa[] scal(tablice tablica1,tablice tablica2,Panstwa[] panstwo)
    {
             int licznik = 0;
                for(int i=0; i<tablica1.klucz.length;i++)
                {for(int j=0; j<tablica2.klucz.length;j++)
                    {
                        
                       if( tablica1.klucz[i].equals(tablica2.klucz[j])  )
                        {
                            panstwo[licznik].Nazwa = tablica1.klucz[i];
                            panstwo[licznik].dlZycia = Integer.parseInt(tablica1.wartosc[i]);
                            panstwo[licznik].SrZar = Integer.parseInt(tablica2.wartosc[j]);
                            licznik++;
                            break;
                        }
                    } 
            }
                return panstwo;
    }
  
    ////////////////////////////////////////////////////
    ///////////////////////Metody///////////////////////
    ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////
    
}
