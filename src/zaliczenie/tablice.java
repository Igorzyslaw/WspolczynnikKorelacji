/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package zaliczenie;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Komputer
 */
public class tablice {
    
    String klucz[];
    String wartosc[];
    
    
    public tablice(int ile,File plik)
    {
        klucz = new String[ile];
        wartosc = new String[ile];
        przepiszDane(plik);
    }
    private void przepiszDane(File plik){
       int licznik = 0;
       BufferedReader br = null;
       try {
            br = new BufferedReader(new FileReader(plik));
        } catch (FileNotFoundException ex) {
            Logger.getLogger(plik.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            for (String line = br.readLine(); line != null; line = br.readLine()) {
                 
                int lineLength = line.length();
                String nazwaPanstwa ="";
                String wartosc = "";
                for(int i=0 ; i<8; i++){
                    char c = line.charAt(i);
                    if( (((int)c >= 65 && (int)c <= 90) || ((int)c >= 97 && (int)c <= 122)) || (int)c == 32 || (int)c == 95 ) /// ignorowanie kodow ascii innych niz standardowe litery
                    {
                        nazwaPanstwa += c;
                    }else
                    {
                        nazwaPanstwa += (char)32; // Jesli znak nie spelnia kryteriow wstaw SPACJE
                    }
                }
                for(int i=8 ; i<lineLength; i++){ 
                    char c = line.charAt(i);
                    if( (int)c >= 48 && (int)c < 58) // ignorowanie wszystkich kodow ASCI procz tych odpowiadajacych liczbom
                    {
                        wartosc += c;
                    }
                }
              if(!"".equals(nazwaPanstwa) && !"".equals(wartosc))
              {
                  this.klucz[licznik] = nazwaPanstwa;
                  this.wartosc[licznik] = wartosc;
                  licznik++;
              }   // Jesli wpis jest poprawny wpisuje dane
              
            }
            
       } catch (IOException ex) {
            Logger.getLogger(plik.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public int sprIleOK(tablice tab1,tablice tab2)
            {
            int licznik = 0;
            
        for (String klucz1 : tab1.klucz) {
                for (String klucz2 : tab2.klucz) {
                    if(klucz1.equals(klucz2))
                    {
                        licznik++;
                        break;
                    }
                }
        }
            
            
            
            
            return licznik;
            }
    
    ////////////////////////////////////////
    ////////////////////////////////////////
    
    
    
    
    
}
